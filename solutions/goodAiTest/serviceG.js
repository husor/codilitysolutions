const  express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const path = require('path');
const fs = require('fs');
const bodyParser = require('body-parser');
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));


/**
 *  gets current date
 */
const getCurrentDate = () => {
    const t = new Date();
    const hour = t.getHours();
    const min = t.getMinutes();
    const d = t.getDate();
    const m = t.getMonth();
    const y = t.getFullYear();

    let fdate = y + "-" + m + "-" + d + " " + hour + ":" + min;
    return fdate;
};

/*
     Lets pretend that this stack
     represents DB which returns
     us items as "stack"
 */
const stack = [];

app.get('/generate', (req, res) => {

    const fdate = getCurrentDate();
    /* for serial number lets use  time as it is going to be unique */
    const serialNumber = new Date().getTime();
    const serialNumberWithDateAndPosition =  serialNumber + ","  + fdate + ", " + stack.length;
    stack.push(serialNumberWithDateAndPosition);
    console.log(JSON.stringify(stack));
    res.send({status: "ok",
        method: "generate",
        message: "Successfully generated a new SN",
        serialNumber: serialNumber,
                serialNumber_: serialNumberWithDateAndPosition
    });
});

app.get('/getActual', (req, res) => {
    const actual = stack[0];
    if (stack.length>0) {
        res.send({
            status: "ok",
            message: "Successfully extracted the active SN",
            method: "getActual",
            serialNumber_: actual
        });
    } else {
        res.send({
            status: "ok",
            message: "The stac is empty",
            method: "getActual",
        });
    }
});


app.get('/delete', (req, res) => {
    let deleted;
    if (stack.length>0){
        deleted = stack.pop();
        console.log(JSON.stringify(stack));
        res.send({status: "ok",
            method: "delete",
            serialNumber_: deleted
        });
    }else {
        res.send({status: "ok",
            method: "delete",
            message: "no sn in DB"
        });
    }

});


app.listen(port, () => console.log(`Listening on port ${port}`));