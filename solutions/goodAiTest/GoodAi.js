const Helper = require("../helpers/Helper");
const helper = new Helper();

class GoodAi {

    solution(x) {
        let container = {finalString: ""};
        sortString(x, container);
        console.log(`SORTED STRING ${container.finalString}`);
        return container.finalString;
    }

    testSolution() {
        console.log("SORT STRING " );
        let x = "Sorting1234";
        let y = this.solution(x);
        console.log(`SORTED STRING ${y}`);
    }


}

const sortString =(input, container)=>{
    let inputArray = input.split("");
    if ( Array.isArray(inputArray) ) {
        let lString = '';
        let uString = '';
        let eN = '';
        let uN = '';
        let finalString = '';
        inputArray.forEach((element) => {
            if(element.match(/[a-z]/)){
                lString = lString.concat(element);
            } else if(element.match(/[A-Z]/)){
                uString = uString.concat(element);
            } else if(typeof  parseInt(element)  == 'number'){
                if(element%2 == 0){
                    eN = eN + element;
                } else {
                    uN = uN + element;
                }
            }
        });
        finalString = finalString + lString + uString + uN + eN;
        container.finalString =  finalString;

    } else {
        throw new Error("type of 'input' param is not array. " +
            "Please, check field requirements for sortString function");
    }
};




module.exports = GoodAi;