const Helper = require("../helpers/Helper");
const helper = new Helper();

class TapeEquilibrium {
    constructor() {
        this.helper = new Helper();
    }

    solution(A) {
        return registerDifference(A, A.length);
    }

    testSolution() {
        let taskName = " TapeEquilibrium.solution(A)";
        helper.logInitTestMessage(taskName);
        let A = [1, 3, 4, 6, 5];
        let expected_value = 3;
        let actual_value = this.solution(A);
        if (expected_value !== actual_value) {
            this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
        } else {
            this.helper.logMessage(expected_value, actual_value);
        }

        (() => {
            this.helper.logInitTestMessage(taskName + " [ empty_and_single ]");
            let A = [];
            let expected_value = 0;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(taskName + " [ with single element  ]");
            let A = [1];
            let expected_value = 0;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(taskName + " [with double element]");
            let A = [-1000, 1000];
            let expected_value = 2000;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(taskName + " [with 3 element]");
            let A = [-3, 1, 1000];
            let expected_value = 1002;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(taskName + " [simple test with negative numbers, length = 5]");
            let A = [-3, -1, -4, -2, -1];
            let expected_value = 3;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();


        (() => {
            this.helper.logInitTestMessage(taskName + " [simple test for [1,0, -1, 1,0,-1]");
            let A = [1, 0, -1, 1, 0, -1];
            let expected_value = 0;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(taskName + " [with larger array]");
            let A = [9, -3, 1, 2, -8, 5, 7, 6];
            let expected_value = 1;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

    }
}


function  registerDifference(A, end){
    if(end>1) {
        let sum = helper.sumArray(A);
        let leftHalfSum = 0;
        let storage = [];
        for (let j = 0; j < end-1; j++) {
            leftHalfSum = leftHalfSum + A[j];
            let rightHalfSum = sum - leftHalfSum;
            let regDifference = Math.abs(rightHalfSum - leftHalfSum);
            storage[j]=regDifference;
        }

        return Math.min(...storage);
    }
    return 0;
}


function returnDiff(sum, value) {
    let sumL = sum - value;
    return Math.abs(sumL - value);
}

function findMin(x, y) {
    if (x < y) {
        return x;
    }
    return y;
}


module.exports = TapeEquilibrium;