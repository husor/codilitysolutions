const Helper = require("../helpers/Helper");

class TestCase1 {
    constructor() {
        this.helper = new Helper();
    }
    solution(A) {
        A.sort((a, b) => { return a - b;});
        return returnMissing(A);
    }

    testSolution() {
        this.helper.logInitTestMessage(" TestCase1.solution(A)");
        let A = [1, 3, 3, 4, 4];
        let expected_value = 2;
        let actual_value = this.solution(A);
        if (expected_value !== actual_value) {
            this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
        } else {
            this.helper.logMessage(expected_value, actual_value);
        }

        (() => {
            this.helper.logInitTestMessage(" TestCase1.solution(A) [with negative number]");
            let A = [2,3, 3, -55, 4, 4];
            let expected_value = 1;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(" TestCase1.solution(A) [with larger array]");
            let A = [-22, 3, 5, 3, 1, 7, 2];
            let expected_value = 4;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

    }
}

function returnMissing(A) {
if(A[A.length-1]>0) {
    let k = 1;
    for (let i = 0; i < A.length - 1; i++) {
        if(A[i]>0) {
            if (A[i] != k ) {
                return k;
            }
            k++;
        }
    }
    return A[A.length-1] + 1;
}
return 1;
}


module.exports = TestCase1;