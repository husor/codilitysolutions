// A non-empty array A consisting of N integers is given. The array contains an odd number of elements, and each element
// of the array can be paired with another element that has the same value, except for one element that is left unpaired.
//
//     For example, in array A such that:
//
// A[0] = 9  A[1] = 3  A[2] = 9
// A[3] = 3  A[4] = 9  A[5] = 7
// A[6] = 9
// the elements at indexes 0 and 2 have value 9,
//     the elements at indexes 1 and 3 have value 3,
//     the elements at indexes 4 and 6 have value 9,
//     the element at index 5 has value 7 and is unpaired.
//     Write a function:
//
// function solution(A);
//
// that, given an array A consisting of N integers fulfilling the above conditions, returns the value of the unpaired element.
//
//     For example, given array A such that:
//
// A[0] = 9  A[1] = 3  A[2] = 9
// A[3] = 3  A[4] = 9  A[5] = 7
// A[6] = 9
// the function should return 7, as explained in the example above.
//
//     Assume that:
//
//     N is an odd integer within the range [1..1,000,000];
// each element of array A is an integer within the range [1..1,000,000,000];
// all but one of the values in A occur an even nu  mber of times.
//     Complexity:
//
// expected worst-case time complexity is O(N);
// expected worst-case space complexity is O(1) (not counting the storage required for input arguments).

const Helper = require("../helpers/Helper");

class OddNumbers {
    constructor() {
        this.helper = new Helper();
    }

    solution(A) {
        A.sort((a,b)=>{return a-b;})
        return quickDelete(A, 0, A.length-1);
    }


    testSolution() {
        this.helper.logInitTestMessage(" OddNumbers.solution(A)");
        let A = [1, 3, 3, 4, 4];
        let expected_value = 1;
        this.solution(A);
        let actual_value = this.helper.sumArray(A);
        if (expected_value !== actual_value) {
            this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
        } else {
            this.helper.logMessage(expected_value, actual_value);
        }

        (() => {
            this.helper.logInitTestMessage(" OddNumbers.solution(A) [with negative number]");
            let A = [3, 3, -55, 4, 4];
            let expected_value = -55;
            this.solution(A);
            let actual_value = this.helper.sumArray(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(" OddNumbers.solution(A) [with larger array]");
            let A = [9, 3, 9, 3, 9, 7, 9];
            let expected_value = 7;
            this.solution(A);
            let actual_value = this.helper.sumArray(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

    }
}
// let inter = 0;
function quickDelete(A, start, end) {
    // console.log(inter + " BAHMANIT: " + A);
    // inter++;
    if(start<end) {
        let piv_position = partition(A, start, end);
        quickDelete(A, start, piv_position - 1);
        quickDelete(A, piv_position + 1, end);
    }
}
function partition(A, start, end) {
    let i = start + 1;
    let piv = A[start];

    for (let j = start + 1; j <= end; j++) {
        if (piv == A[j]) {
            change(A, start, j);
            i++;
            break;
        }
    }
    swap(A,start, i-1);
    return i - 1;
}
function swap(A, x,y){
    let key = A[x];
    A[x] = A[y];
    A[y] = key;
}

function change(A, a, b) {
    A[a] = 0;
    A[b] = 0;
}

module.exports = OddNumbers;