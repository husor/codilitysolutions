const Helper = require("../helpers/Helper");
const helper = new Helper();

class TimeComplexity {
    constructor() {
        this.helper = new Helper();
    }
    solution(A){
        let k =  1;
        let len = A.length;
            A.sort((a, b) => {
                return a - b;
            });
            if(len>0 && A[0]==1) {
                for (let i = 0; i < len; i++) {
                    k = A[i] + 1;
                    if (A[i + 1] != k) {
                        return k;
                    }
                }
                return A[len-1] + 1;
            }
            return k;
    }

    testSolution() {
        let taskName = " TimeComplexity.solution(A)";
        helper.logInitTestMessage(taskName);
        let A = [1, 3, 4, 6, 5];
        let expected_value = 2;
        let actual_value = this.solution(A);
        if (expected_value !== actual_value) {
            this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
        } else {
             this.helper.logMessage(expected_value, actual_value);
        }

        (() => {
            this.helper.logInitTestMessage(taskName + " [ empty_and_single ]");
            let A = [];
            let expected_value = 1;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(taskName + " [ with single element  ]");
            let A = [1];
            let expected_value = 2;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(taskName + " [ missing_first  ]");
            let A = [3, 2, 4, 6, 5];
            let expected_value = 1;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(taskName + " [ missing_last  ]");
            let A = [1, 3, 2, 4, 6, 5];
            let expected_value = 7;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(taskName + " [with double element]");
            let A = [3,1];
            let expected_value = 2;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(taskName + " [with larger array]");
            let A = [9, 3, 1, 2, 8, 5, 7, 6];
            let expected_value = 4;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

    }
}


module.exports = TimeComplexity;