const Helper = require("../helpers/Helper");
const helper = new Helper();

class QuickSort {

    solution(A){
       if(helper.checkTypeAndContentForArray(A, "number")){
           let start = 0;
           let end = A.length-1;
           quickSort(A, start,  end);
           return true;
       }
       return false;
    }

    testSolution(){
        helper.logInitTestMessage(" QuickSort.solution(A)");
        let A = [1,-3,5,4,4,99];
        let expected_value = [99,5,4,4,1,-3];
        this.solution(A);
        let actual_value = A;
        if( !helper.compareArrays(expected_value,actual_value)){
            helper.logMessage(expected_value, "\x1b[31m" + actual_value);
        }else{
            helper.logMessage(expected_value, actual_value);
        }
    }


}

function partition(A, start, end){
    let i = start + 1;
    let piv = A[start];
    for (j=start+1; j<=end; j++){
        if(A[j]>piv){
            swap(A,i,j);
            i++;
        }
    }
    swap(A,start, i-1);
    return i-1;
}

function quickSort(A, start,  end){
    if(start<end){
        let piv_position = partition(A, start, end);
        quickSort(A, start, piv_position-1);
        quickSort(A, piv_position+1, end);
    }
}

function swap(A, x,y){
   let key = A[x];
   A[x] = A[y];
   A[y] = key;
}

module.exports = QuickSort;