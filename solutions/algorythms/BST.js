const Helper = require("../helpers/Helper");
const helper = new Helper();

class BST {

    solution(root) {
         isBst(root);
    }

    testSolution() {
        let node13 = new Node(13, null, null);
        let node14 = new Node(14, node13, null);
        let node10 = new Node(10, null, node14);
        let node4 = new Node(4, null, null);
        let node7 = new Node(7, null, null);
        let node6 = new Node(6, node4, node7);
        let node1 = new Node(1, null, null);
        let node3 = new Node(3, node1, node6);
        let root = new Node(8, node3, node10);
        this.solution(root);
        // let actual_value = A;
        // if (!helper.compareArrays(expected_value, actual_value)) {
        //     helper.logMessage(expected_value, "\x1b[31m" + actual_value);
        // } else {
        //     helper.logMessage(expected_value, actual_value);
        // }
    }


}


class Node {
    constructor(data, left, right) {
        this.data = data;
        this.left = left;
        this.right = right;
    }
}


function isBst(root){
    console.log(JSON.stringify(root));
    try{
        if(root.data){
            console.log(`*********root ${root.data} *********`);
            if(root.data > root.left.data && root.data < root.right.data){
                if(root.data > root.left.right.data  )
                {
                    isBst(root.left);
                    isBst(root.right);
                }else {
                    console.log("NOT THE BEST SEARCH TREE");
                    return;
                }
            } else {
                console.log("NOT A SEARCH TREE");
            }

        }
    } catch (e) {
        console.log(`*********root is NULL *********`);
    }
}

module.exports = BST;