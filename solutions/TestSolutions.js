const BST = require("./algorythms/BST");

const Helper = require("./helpers/Helper.js");
const OddNumbers = require("./arrays/OddNumbers");
const OddNumbersAlters = require("./arrays/OddNumberssAlter");
const TimeComplexity = require("./arrays/TimeComplexity");
const TapeEquilibrium = require("./arrays/TapeEquilibrium");
const PermCheck = require("./counting_elements/PermCheck");
const FrogRiverOne = require("./counting_elements/FrogRiverOne");

const QuickSort = require("./algorythms/QuickSort");
const GoodAi = require("./goodAiTest/GoodAi");

const TestCase1 = require("./arrays/TestCase1");

//Helper
let helper = new  Helper();
helper.testSumArray();
helper.testCheckTypeAndContentForArray();
//Arrays : OddNumbers
// let oddNumbers = new OddNumbers();
// oddNumbers.testSolution();
let oddAlter = new OddNumbersAlters();
oddAlter.testSolution();
let timeComplexity = new TimeComplexity();
timeComplexity.testSolution();
let tapeEquilibrium = new TapeEquilibrium();
tapeEquilibrium.testSolution();
//Counting numbers
let permCheck = new PermCheck();
permCheck.testSolution();

let frogRiverOne = new FrogRiverOne();
frogRiverOne.testSolution();


// let testCase = new TestCase1();
// testCase.testSolution();

//Algorythms
let quickSort = new QuickSort();
quickSort.testSolution();


//test
let bst = new  BST();
bst.testSolution();

//GoodAi
let gai = new GoodAi;
gai.testSolution();





