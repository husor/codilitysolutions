// A non-empty array A consisting of N integers is given. The array contains an odd number of elements, and each element
// of the array can be paired with another element that has the same value, except for one element that is left unpaired.
//
//     For example, in array A such that:
//
// A[0] = 9  A[1] = 3  A[2] = 9
// A[3] = 3  A[4] = 9  A[5] = 7
// A[6] = 9
// the elements at indexes 0 and 2 have value 9,
//     the elements at indexes 1 and 3 have value 3,
//     the elements at indexes 4 and 6 have value 9,
//     the element at index 5 has value 7 and is unpaired.
//     Write a function:
//
// function solution(A);
//
// that, given an array A consisting of N integers fulfilling the above conditions, returns the value of the unpaired element.
//
//     For example, given array A such that:
//
// A[0] = 9  A[1] = 3  A[2] = 9
// A[3] = 3  A[4] = 9  A[5] = 7
// A[6] = 9
// the function should return 7, as explained in the example above.
//
//     Assume that:
//
//     N is an odd integer within the range [1..1,000,000];
// each element of array A is an integer within the range [1..1,000,000,000];
// all but one of the values in A occur an even nu  mber of times.
//     Complexity:
//
// expected worst-case time complexity is O(N);
// expected worst-case space complexity is O(1) (not counting the storage required for input arguments).

const Helper = require("../helpers/Helper");

class PermCheck {
    constructor() {
        this.helper = new Helper();
    }

    solution(A) {
          return permCheck(A);
    }


    testSolution() {
        this.helper.logInitTestMessage(" PermCheck.solution(A)");
        let A = [1, 5, 3, 4, 2];
        let expected_value = 1;
        let actual_value = this.solution(A);
        if (expected_value !== actual_value) {
            this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
        } else {
            this.helper.logMessage(expected_value, actual_value);
        }

        (() => {
            this.helper.logInitTestMessage(" PermCheck.solution(A) [with negative number]");
            let A = [3, 3, -55, 4, 4];
            let expected_value = 0;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(" PermCheck.solution(A) [with larger array]");
            let A = [10,1, 7, 3, 5, 2, 4, 6];
            let expected_value = 0;
            let actual_value = this.solution(A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

    }
}


function permCheck(A){
    let len = A.length;
    A.sort((a,b)=>{ return a-b;});
    if(A[0]==1) {
        for (let i = 0; i < len - 1; i++) {
            if (A[i + 1] != A[i] + 1) {
                return 0;
            }
        }
        return 1;
    }
    return 0;
}

module.exports = PermCheck;