// A non-empty array A consisting of N integers is given. The array contains an odd number of elements, and each element
// of the array can be paired with another element that has the same value, except for one element that is left unpaired.
//
//     For example, in array A such that:
//
// A[0] = 9  A[1] = 3  A[2] = 9
// A[3] = 3  A[4] = 9  A[5] = 7
// A[6] = 9
// the elements at indexes 0 and 2 have value 9,
//     the elements at indexes 1 and 3 have value 3,
//     the elements at indexes 4 and 6 have value 9,
//     the element at index 5 has value 7 and is unpaired.
//     Write a function:
//
// function solution(A);
//
// that, given an array A consisting of N integers fulfilling the above conditions, returns the value of the unpaired element.
//
//     For example, given array A such that:
//
// A[0] = 9  A[1] = 3  A[2] = 9
// A[3] = 3  A[4] = 9  A[5] = 7
// A[6] = 9
// the function should return 7, as explained in the example above.
//
//     Assume that:
//
//     N is an odd integer within the range [1..1,000,000];
// each element of array A is an integer within the range [1..1,000,000,000];
// all but one of the values in A occur an even nu  mber of times.
//     Complexity:
//
// expected worst-case time complexity is O(N);
// expected worst-case space complexity is O(1) (not counting the storage required for input arguments).

const Helper = require("../helpers/Helper");

class FrogRiverOne {
    constructor() {
        this.helper = new Helper();
    }

    solution(X, A) {
        return compute(X, A);
    }


    testSolution() {
        this.helper.logInitTestMessage(" FrogRiverOne.solution(A)");
        let A = [1, 5, 3, 4, 2];
        let expected_value = 4;
        let actual_value = this.solution(5, A);
        if (expected_value !== actual_value) {
            this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
        } else {
            this.helper.logMessage(expected_value, actual_value);
        }

        (() => {
            this.helper.logInitTestMessage(" FrogRiverOne.solution(A) [with .... number]");
            let A = [3, 3, 1, 4, 4];
            let expected_value = -1;
            let actual_value = this.solution(4, A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(" FrogRiverOne.solution(A) [with larger array]");
            let A = [5, 1, 7, 3, 5, 2, 4, 6];
            let expected_value = 7;
            let actual_value = this.solution(7, A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();

        (() => {
            this.helper.logInitTestMessage(" FrogRiverOne.solution(A) [custom test]");
            let A = [1, 3, 1, 4, 2, 3, 5, 4];
            let expected_value = 6;
            let actual_value = this.solution(5, A);
            if (expected_value !== actual_value) {
                this.helper.logMessage(expected_value, "\x1b[31m" + actual_value);
            } else {
                this.helper.logMessage(expected_value, actual_value);
            }
        })();
    }
}


// function to2dArray(A) {
//     let len = A.length;
//     let B = [];
//     for (let i = 0; i < len; i++) {
//         B[i] = [i, A[i]];
//     }
//     return B;
// }


// function compute(X, A) {
//      let B = [];
//      let j = 0;
//      let time = 0;
//
//      for(let i = 0; i<A.length; i++){
//          if(j<X) {
//              if (!B.includes(A[i])) {
//                  let index = A[i] - 1;
//                  B[index] = A[i];
//                  j++;
//                  time = i;
//              }
//          }
//      }
//
//         if(sumInc(X)== sumArray(B)){
//             return time;
//         }else{
//             return -1;
//         }
//
// }
function compute(X, A) {
    let B = [];
    let j = 0;
    let time = 0;

    for(let i = 0; i<A.length; i++){
        if(j<X) {
            let index = A[i] - 1;
            if (B[index]!=(A[i])) {
                B[index] = A[i];
                j++;
                time = i;
            }
        }
    }

    if(sumInc(X)== sumArray(B)){
        return time;
    }else{
        return -1;
    }

}


function sumInc(X){
    let x = 0
    for(let i = 0; i<X; i++){
        x = x+i +1;
    }
    return x;
}

function sumArray(A){
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
   return A.reduce(reducer);
}
module.exports = FrogRiverOne;