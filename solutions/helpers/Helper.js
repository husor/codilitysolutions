TEST_MESSAGE = "\x1b[34mTEST CASE : ";

class Helper{
    constructor(){
    }

    checkTypeAndContentForArray(array, type){
        if(Array.isArray(array)){
            if(!array.every((e)=>{ return typeof e == type; })){
                console.log("\x1b[31mArray content is not matching required type '" + type  +   "'");
                return false;
            }
            return true;
        }else{
            console.log("You have provided " + typeof array + "instead of 'array'");
            return false;
        }
    }
    sumArray(A) {
        return A.reduce((a, b) => a + b, 0);
    }
    logMessage(expected_value, actual_value) {
        console.log("\x1b[32mRESULT : " +
            "\n\x1b[0mExpected value: " + expected_value +
            "\nActual result: " + actual_value);
    }
    logInitTestMessage(methodName) {
        console.log(TEST_MESSAGE + " " +methodName);
    }

    compareArrays(a,b){
        if(Array.isArray(a) && Array.isArray(b)) {
            if (a.length == b.length) {
                for ( let i = 0; i < a.length; i++) {
                    if (a[i] == b[i]) {
                        continue;
                    } else {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }else {
            console.error("You've not provided inputs of right types");
            return false;
        }
    }

//tests
    testSumArray(){
        // console.log(TEST_MESSAGE + " sumArray(A)");
        this.logInitTestMessage(" sumArray(A)");
        let A = [1,3,-4,4];
        let expected_value = 4;
        let actual_value = this.sumArray(A);
        this.logMessage(expected_value, actual_value);
    }

    testCheckTypeAndContentForArray(){
        this.logInitTestMessage("checkTypeAndContentForArray(A, \"number\")");
        let A = [1,3,-4,4];
        let expected_value = true;
        let actual_value = this.checkTypeAndContentForArray(A, "number");
        this.logMessage(expected_value, actual_value);

        (()=>{
            this.logInitTestMessage("checkTypeAndContentForArray(A, \"number\")" + " (error scenario) ");
            let A = [1,"4",-4,4];
            let expected_value = false;
            let actual_value = this.checkTypeAndContentForArray(A, "number");
            this.logMessage(expected_value, actual_value);
        })()
    }
}


module.exports = Helper;